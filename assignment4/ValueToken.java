/**
 * this class sets value tokens  - value numbers
 */
public class ValueToken extends CalcToken {
	
	private double val;
	
	/**
	 * the empty constructor
	 */
	public ValueToken(){
		val = 0;
	}
	
	/**
	 * the non empty constructor - get double number
	 * @param val the value to save
	 */
	public ValueToken(double val){
		this.val = val;
	}
	
	/**
	 * getter method
	 * @return the value of the token
	 */
	public double getValue(){
		return val;
	}
	
	/**
	 * this method print the string number
	 */
	public String toString(){		
		String res = new String("");
		res = val+res;
		return res;
	
	}

}
