/**
 * this class inherited from BinaryOp use to describe the divide operation.
 * its include 3 method
 */
public class DivideOp extends BinaryOp {
	
	/**
	 * this method calculate the devide arithmetic function and return the answer
	 * @param left - the left number in the equation
	 * @param right - the right number in the equation
	 */
	public double operate(double left, double right){
		if (right==0) throw new java.lang.ArithmeticException("/devide by zero");
		return left/right;
	}
	
	/**
	 * this method print the string operator
	 */
	public String toString(){
		return "/";
	}
	
	/**
	 * this method return the precedence of the operator
	 */
	public double getPrecedence() {
		return 1;
	}

}
