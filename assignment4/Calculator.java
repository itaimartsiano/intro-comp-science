/**
 * abstract class describes the methods of subclass must implement
 */
public abstract class Calculator {
	/**
	 * @param res use to save the result of last equation
	 */
	protected double res;
	
	/**
	 * this abstract method get equation by string, evaluate the numbers and return the result to res/
	 * @param expr is the valid equation the user enter/
	 */
	public abstract void evaluate (String expr);
	
	/**
	 * this method return the current result of the eqution from @param res
	 */
	protected double getCurrentResult(){
		return res;
	}
}
