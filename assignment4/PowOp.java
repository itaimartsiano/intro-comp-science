/**
 * this class inherited from BinaryOp use to describe the power operation.
 * its include 3 method
 */
public class PowOp extends BinaryOp {
	
	/**
	 * this method calculate the power arithmetic function and return it result
	 * @param left - the left number in the equation
	 * @param right - the right number in the equation
	 */
	public double operate(double left, double right){	//to check if is ok to use math op
		return Math.pow(left, right);
	}
	
	
	/**
	 * this method print the string operator
	 */
	public String toString(){
		return "^";
	}
	
	/**
	 * this method return the precedence of the operator
	 */
	public double getPrecedence() {
		return 2;
	}

}
