/**
 * This is a testing framework. Use it extensively to verify that your code is working
 * properly.
 */
public class Tester {

	private static boolean testPassed = true;
	private static int testNum = 0;
	
	/**
	 * This entry function will test all classes created in this assignment.
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		
		testAddOp();
		testDivideOp();
		testMultiplyOp();
		testPowOp();
		testSubtractOp();
		testValueToken();
		testExpTokenizer();
		testPeekableStackAsArray();
		testPrefixCalculator();
		testInfixCalculator();
		
		// Notifying the user that the code have passed all tests. 
		if (testPassed) {
			System.out.println("All " + testNum + " tests passed!");
		}
	}

	/**
	 * This utility function will count the number of times it was invoked. 
	 * In addition, if a test fails the function will print the error message.  
	 * @param exp The actual test condition
	 * @param msg An error message, will be printed to the screen in case the test fails.
	 */
	private static void test(boolean exp, String msg) {
		testNum++;
		
		if (!exp) {
			testPassed = false;
			System.out.println("Test " + testNum + " failed: "  + msg);
		}
	}
	
	/**
	 * Checks the ValueToken class.
	 */
	private static void testValueToken() {
		
		ValueToken t1 = new ValueToken(5.1);
		ValueToken t2 = new ValueToken();
		
		test(t1.getValue() == 5.1, "Value should be 5.1.");
		test(t1.toString().equals("5.1"), "Value toString should be 5.1.");	
		test(t2.toString().equals("0.0"), "there is a problem in empty costructor");
	}

	/**
	 * Checks the PrefixCalculator class.
	 */
	private static void testPrefixCalculator() {
		
		PrefixCalculator c1 = new PrefixCalculator();
		c1.evaluate("- / ^ 2 3 * 4 2 -7");
		test(c1.getCurrentResult()==8, "the prefixcalculator is not warking");
		c1.evaluate("^ 99 0");
		test(c1.getCurrentResult()==1, "the prefixcalculator is not warking");
		c1.evaluate("+ 3 / 0 9");
		test(c1.getCurrentResult()==3, "the prefixcalculator is not warking");
		c1.evaluate("+ 3 / 0 9");
		test(c1.getCurrentResult()==3, "the prefixcalculator is not warking");
		c1.evaluate("+ 6 - 4 * 2 3");
		test(c1.getCurrentResult()==4, "the prefixcalculator is not warking");
		c1.evaluate("- ^ 2 3 -4");
		test(c1.getCurrentResult()==12, "the prefixcalculator is not warking");
		c1.evaluate("- * + 2 3 4 1");
		test(c1.getCurrentResult()==19, "the prefixcalculator is not warking");
		c1.evaluate("- ^ 2 3 -4");
		test(c1.getCurrentResult()==12, "the prefixcalculator is not warking");
	}
	
	/**
	 * tests the InfixCalculator class.
	 */
	private static void testInfixCalculator() {
		
		InfixCalculator c1 = new InfixCalculator();
		c1.evaluate("4 + 8 * ( 2 )");
		test(c1.getCurrentResult()==20, "the InfixCalculator is not warking");
		c1.evaluate("( 1 ) + ( 1 )");
		test(c1.getCurrentResult()==2, "the InfixCalculator is not warking");
		c1.evaluate("3 * 2 ^ 2");
		test(c1.getCurrentResult()==12, "the InfixCalculator is not warking");
		c1.evaluate("( 22 / 11 ) * 3 ^ 3 + 6");
		test(c1.getCurrentResult()==60, "( 22 / 11 ) * 3 ^ 3 + 6 result should be 60");
		c1.evaluate("6 ^ 0 / 5");
		test(c1.getCurrentResult()==(1.0/5.0), "6 ^ 0 / 5 result sould be 0.2");
		c1.evaluate("( ( 100 * 9 / 30 ) / 6 ) ^ 2");
		test(c1.getCurrentResult()==25, "( ( 100 * 9 / 30 ) / 6 ) ^ 2 result sould be 25");
		c1.evaluate("100 + 4 - 4 * 3");
		test(c1.getCurrentResult()==92, "100 + 4 - 4 * 3 result sould be 92");
		c1.evaluate("100 + ( 4 - 4 ) * 3");
		test(c1.getCurrentResult()==100, "100 + ( 4 - 4 ) * 3 result sould be 100");
		c1.evaluate("( ( 5 - 9 ) / ( 0.25 * 8 ) + ( 2 - -2 ) )");
		test(c1.getCurrentResult()==2, "( ( 5 - 9 ) / ( 0.25 * 8 ) + ( 2 - -2 ) ) result should be 2");
		c1.evaluate("( 9 + ( ( ( 0 ) ) ) )");
		test(c1.getCurrentResult()==9, "( 9 + ( ( ( 0 ) ) ) ) result should be 9");
		c1.evaluate("( ( 10 / ( 2 ^ 2 ) / 3.75 ) * ( 6 ) ) ^ ( 0.5 * ( ( ( 10.5 - 6.5 ) ) ) )");
		test(c1.getCurrentResult()==16, "the result should be 16");
		c1.evaluate("5 -");
		c1.evaluate("( ( ( (");
		c1.evaluate("6 ^ 7 %");
		c1.evaluate(") 4 (4)");
	}
	
	
	
	/**
	 * tests the add operator
	 */
	private static void testAddOp() {
		AddOp c1 = new AddOp();
		test(c1.operate(0, 9)==9, "the add operator is not working");
		test(c1.operate(-4, 9)==5, "the add operator is not working");
		test(c1.operate(-3, -3)==-6, "the add operator is not working");
	}
	
	
	/**
	 * tests the Divide operator
	 */
	private static void testDivideOp() {
		DivideOp c1 = new DivideOp();
		test(c1.operate(12, -6)==-2, "the divide operator is not working");
		test(c1.operate(0, 10)==0, "the divide operator is not working");
		test(c1.operate(-8, -2)==4, "the divide operator is not working");
	}
	
	/**
	 * tests the Multiply operator
	 */
	private static void testMultiplyOp() {
		MultiplyOp c1 = new MultiplyOp();
		test(c1.operate(12, -2)==-24, "the Multiply operator is not working");
		test(c1.operate(9, 0)==0, "the Multiply operator is not working");
		test(c1.operate(-3, -2)==6, "the Multiply operator is not working");
	}
	
	/**
	 * tests the Power operator
	 */
	private static void testPowOp() {
		PowOp c1 = new PowOp();
		test(c1.operate(-12, 2)==144, "the Power operator is not working");
		test(c1.operate(8, 0)==1, "the Power operator is not working");
		test(c1.operate(2, -2)==(1.0/4.0), "the Power operator is not working");
	}
	
	/**
	 * tests the Subtract operator
	 */
	private static void testSubtractOp() {
		SubtractOp c1 = new SubtractOp();
		test(c1.operate(12, -2)==14, "the Subtract operator is not working");
		test(c1.operate(-4 , -3)==-1, "the Subtract operator is not working");
		test(c1.operate(12, 2)==10, "the Subtract operator is not working");
	}
	
	/**
	 * tests the Subtract operator
	 */
	private static void testExpTokenizer() {
		ExpTokenizer c1 = new ExpTokenizer("3 + * / - ( )",false);
		test(c1.countTokens()==7, "the ExpTokenizer didnt how to count the tokens");
		test(c1.nextElement() instanceof CloseBracket==true, "the ExpTokenizer didnt recognize the element");
		test(c1.nextElement() instanceof OpenBracket==true, "the ExpTokenizer didnt recognize the element");
		test(c1.nextElement() instanceof SubtractOp==true, "the ExpTokenizer didnt recognize the element");
		test(c1.nextElement() instanceof DivideOp==true, "the ExpTokenizer didnt recognize the element");
		test(c1.nextElement() instanceof MultiplyOp==true, "the ExpTokenizer didnt recognize the element");
		test(c1.nextElement() instanceof AddOp==true, "the ExpTokenizer didnt recognize the element");
		test(((ValueToken) c1.nextElement()).getValue()==3, "the ExpTokenizer do not pop out the next element");
		test(c1.hasNext()==false, "the ExpTokenizer think there is more elements but its incorrect");
		test(c1.countTokens()==0, "the ExpTokenizer didnt how to count the tokens");
	}
	
	/**
	 * tests the PeekableStackAsArray class
	 */
	private static void testPeekableStackAsArray() {
		
		PeekableStackAsArray c1 = new PeekableStackAsArray();
		ValueToken token = new ValueToken(9);
		c1.push(token);
		test(c1.isEmpty()==false, "the stack dont have the is empty method");
		test(((ValueToken)(c1.peek(0))).getValue() ==9.0, "the peek method is not working");
		c1.pop();
		test(c1.isEmpty() ==true, "the pop method dont change the stack size");
		c1.push(token);
		test(c1.size() == 1, "the size method is not working");
		test(c1.stackContents().equalsIgnoreCase("9.0") == true, "the stack contant method is not working");
		c1.clear();
		test(c1.isEmpty() ==true, "the pop method dont change the stack size");
	}
	
}
