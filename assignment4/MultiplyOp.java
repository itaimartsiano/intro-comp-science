
/**
 * this class inherited from BinaryOp use to describe the multiply operation.
 * its include 3 method
 */
public class MultiplyOp extends BinaryOp{
	
	/**
	 * this method calculate the multiply arithmetic function and return the its answer.
	 * @param left - the left number in the equation
	 * @param right - the right number in the equation
	 */
	public double operate(double left, double right){
		return left*right;
	}
	
	/**
	 * this method print the string operator
	 */
	public String toString(){
		return "*";
	}
	
	/**
	 * this method return the precedence of the operator
	 */
	public double getPrecedence() {
		return 1;
	}

}
