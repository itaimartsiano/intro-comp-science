/**
 * this class inherited from CalcToken and describe the ) which symbolize the end of sub equation
 */
public class CloseBracket extends CalcToken {

	/**
	 * this method print the string operator
	 */
	public String toString() {
		return ")";
	}

}
