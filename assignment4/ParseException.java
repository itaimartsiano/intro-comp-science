/**
 * this abstract class extends runtime exception and use to set message when
 * the user enter non valid expression
 */
public class ParseException extends RuntimeException {
	/**
	 * the empty constructor
	 * @param message is the message which throw when call this empty constructor
	 */
	public ParseException (String message){
		super(message);
	}

}
