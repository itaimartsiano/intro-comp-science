/**
 * this class extend StackAsArray class and implements PeekableStack
 * this class sets special stack, you can peek to the stack without take out tokens.
 */
import java.util.EmptyStackException;
public class PeekableStackAsArray extends StackAsArray implements PeekableStack {
	
	/**
	 * this method return the size of the stack
	 */
	public int size(){
		return size;
	}

	
	/** 
	 * Return the i-th item from the top of the stack, where the top
	 *  is the 0-th item.
	 * Precondition: i is strictly less than the stack's size AND i >= 0.
	 * @return the i-th stack item.
 	 * @throws EmptyStackException if trying to peek at an element which is not in the stack.
	 */
	public Object peek(int i){
		Object argument = null;
		if (i>size) throw new EmptyStackException(); //throw exception if there is no such place in the stack
		if (i < elements.length){
			argument = elements[i];
		}
		return argument;
	}

	/**
	 * Returns a newline(\n)-delimited (i.e. between adjacent elements)
	 *  String of the stack's contents, in order from top downwards.  
	 *  The String for each element is the result of that element's
	 *  toString() method. If the stack is empty, return the empty string.
	 * @return String representation of the stack contents
	 */
	public String stackContents(){
		
		String content = "";
		int i = 0;
		while (i<elements.length && elements[i] != null){
			content = content + elements[i];
			i++;
		}
		return content;
	}

	/**
	 * Empty the stack of all its contents.
	 */
	public void clear(){
		int i = 0;
		size = 0;
		while (i<elements.length){
			elements[i] = null;
			i++;
		}
		
	}

	

}
