/**
 * this class inherited from BinaryOp use to describe the sum operation.
 * its include 3 method
 */

public class AddOp extends BinaryOp {
	/**
	 * this method calculate the sum arithmetic function and return the sum
	 * @param left - the left number in the equation
	 * @param right - the right number in the equation
	 */
	public double operate(double left, double right){
		return left+right;
	}
	
	/**
	 * this method print the string operator
	 */
	public String toString(){
		return "+";
	}
	
	/**
	 * this method return the precedence of the operator
	 */
	public double getPrecedence() {
		return 0.0;
	}

}
