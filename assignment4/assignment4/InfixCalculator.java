/**
 *	this class is the Infix Calculator, use to calculate the regular expression
 */
public class InfixCalculator extends Calculator {
	
	protected PeekableStackAsArray mainstack;
	
	/**
	 * the empty constructor
	 */
	public InfixCalculator(){
		this.mainstack = new PeekableStackAsArray();
	}
	
	
	/**
	 * this function will evaluate the expression by using collapse method(appear in this class)
	 * @param expr is the expression the user want to calculate
	 * assume the expression is valid
	 */
	public void evaluate(String expr) {
	
		ExpTokenizer expression = new ExpTokenizer (expr, true);
		mainstack.clear();	res = 0;
		
		while (expression.hasNext()){	/** while there is more tokens in the expression */
			
			CalcToken token = expression.nextElement();
			/**
			 * there are 5 'if' that checks that expression is valid, if not they will stop the program immediately
			 */
			if (!mainstack.isEmpty() && token instanceof ValueToken && mainstack.peek(mainstack.size()-1) instanceof ValueToken)
					throw new ParseException("Can't perform two numbers continusly: " + token.toString());
			
			if ((token instanceof OpenBracket && !expression.hasNext()) ||
				(mainstack.isEmpty() && token instanceof CloseBracket)||
				((token instanceof BinaryOp && !expression.hasNext()))) 
				throw new ParseException("Can't perform the operator: " + token.toString());
			
			if (token instanceof CloseBracket && mainstack.size()>1 && 
				!(mainstack.peek(mainstack.size()-2) instanceof OpenBracket) && 
				!(mainstack.peek(mainstack.size()-2) instanceof BinaryOp) &&
				!(mainstack.peek(mainstack.size()-1) instanceof CloseBracket) && !expression.hasNext())
				throw new ParseException("Can't perform the operator: " + token.toString());
			
			if (token instanceof BinaryOp && (mainstack.isEmpty()))
					throw new ParseException("Can't perform the operator: " + token.toString());
			
			if (mainstack.size()>1 && (mainstack.peek(mainstack.size()-1) instanceof OpenBracket) &&
				((!(token instanceof ValueToken) && !(token instanceof OpenBracket)) ||
				(!(mainstack.peek(mainstack.size()-2) instanceof BinaryOp) && !(mainstack.peek(mainstack.size()-2) instanceof OpenBracket))))
					throw new ParseException("Can't perform the operator: " + token.toString());
			
			collapse(token);
			mainstack.push(token);
		}
		
		if (!expression.hasNext()){
			while (mainstack.size()>1)	collapse(null);
		}
		
		/**
		 * check if the stack is empty or the answer is not legible/	
		 */
		if (mainstack.isEmpty())throw new ParseException ("The stack is empty");
		if (!(mainstack.peek(0) instanceof ValueToken)) throw new ParseException
			("SYNTAX ERORR: '" + mainstack.pop().toString() + "' cannot be answer");
		
		res =  ((ValueToken) mainstack.pop()).getValue();
			
	} //evaluate
	
	/**
	 * this method use to decide what to do with an argument, calculate two numbers and operation,
	 * delete brackets or just do nothing
	 * @param token is the next calculator token that the method have to take into account.
	 */
	private void collapse(CalcToken token){
		
		boolean endcollapse = false;	
		
		while (!endcollapse && mainstack.size()>2){
			CalcToken E0 = (CalcToken) mainstack.peek(mainstack.size()-1);
			CalcToken E1 = (CalcToken) mainstack.peek(mainstack.size()-2);
			CalcToken E2 = (CalcToken) mainstack.peek(mainstack.size()-3);
					
			if (E0 instanceof ValueToken & E2 instanceof ValueToken & E1 instanceof BinaryOp && 
				(!(token instanceof BinaryOp) || token instanceof BinaryOp && ((BinaryOp)E1).getPrecedence() >= ((BinaryOp)token).getPrecedence())){
					ValueToken left = (ValueToken) E2;
					ValueToken right = (ValueToken) E0;
					mainstack.pop(); mainstack.pop() ; mainstack.pop();
					if (E1 instanceof AddOp){
						ValueToken num = new ValueToken(((AddOp) E1).operate(left.getValue(), right.getValue()));
						mainstack.push(num);
					}else	
						if (E1 instanceof MultiplyOp){
							ValueToken num = new ValueToken(((MultiplyOp) E1).operate(left.getValue(), right.getValue()));
							mainstack.push(num);
						}else	
							if (E1 instanceof DivideOp){
								ValueToken num = new ValueToken(((DivideOp) E1).operate(left.getValue(), right.getValue()));
								mainstack.push(num);
							}else	
								if (E1 instanceof PowOp){
									ValueToken num = new ValueToken(((PowOp) E1).operate(left.getValue(), right.getValue()));
									mainstack.push(num);
								}else	
									if (E1 instanceof SubtractOp){
										ValueToken num = new ValueToken(((SubtractOp) E1).operate(left.getValue(), right.getValue()));
										mainstack.push(num);
									}
					
				
			}else
				if (E2 instanceof OpenBracket & E0 instanceof CloseBracket & E1 instanceof ValueToken){
					mainstack.pop(); mainstack.pop() ; mainstack.pop();
					mainstack.push(E1);
				}else endcollapse = true;
		}
			
	} //collapse

}
