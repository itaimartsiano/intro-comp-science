/**
 * this class sets prefix calculator method
 */
public class PrefixCalculator extends Calculator {
	
	protected StackAsArray mainstack;
	
	/**
	 * the empty constructor
	 */
	public PrefixCalculator(){
		mainstack = new StackAsArray();
	}
	
	/**
	 * this method evaluate the expression
	 * assume the expression is valid
	 * @param expr the expression which user enter
	 */
	public void evaluate(String expr){
		
		ExpTokenizer tempexpr = new ExpTokenizer(expr,false);
		CalcToken nelement;
		while (tempexpr.hasNext()){
			nelement = tempexpr.nextElement();
			if (!(nelement instanceof ValueToken)){
				ValueToken left = (ValueToken)mainstack.pop();
				ValueToken right = (ValueToken)mainstack.pop();
				if (nelement instanceof AddOp){
					ValueToken num = new ValueToken(((AddOp) nelement).operate(left.getValue(), right.getValue()));
					mainstack.push(num);
				}
				else	
					if (nelement instanceof MultiplyOp){
						ValueToken num = new ValueToken(((MultiplyOp) nelement).operate(left.getValue(), right.getValue()));
						mainstack.push(num);
					}	
					else	
						if (nelement instanceof DivideOp){
							ValueToken num = new ValueToken(((DivideOp) nelement).operate(left.getValue(), right.getValue()));
							mainstack.push(num);
						}		
						else	
							if (nelement instanceof PowOp){
								ValueToken num = new ValueToken(((PowOp) nelement).operate(left.getValue(), right.getValue()));
								mainstack.push(num);
							}
							else	
								if (nelement instanceof SubtractOp){
									ValueToken num = new ValueToken(((SubtractOp) nelement).operate(left.getValue(), right.getValue()));
									mainstack.push(num);
								}
			}
				else {
					mainstack.push(nelement);
				}
		}
		if (mainstack != null & !mainstack.isEmpty()){
			res =  (double)(((ValueToken) mainstack.pop()).getValue());
		}
			
	}//evaluate

}
