
public class Snowflake {	//this class sets snow flake fields and methods
	
	private BasicStar centerstar;		//this field is the central star
	private int depth;					//this field set the depth of the snow flake(the number of basic star circles)

	public Snowflake(){		//the empty constructor
		this.centerstar = new BasicStar();
		this.depth = 4;
	}
	
	public Snowflake(BasicStar center, int depth){		//the non empty constructor - inserting manually parameters
		this();
		if (centerstar != null && depth>-1){
			centerstar = center;
			this.depth = depth;
		}
	}
	
	public BasicStar getCenterstar(){	//return values of center basic star
		return centerstar;
	}
	
	public int getdepth(){				//return value of depth
		return depth;
	}
	
	public Pixel getcenterpixel(){		//return pixels values of center basic star
		return centerstar.getCenter();
	}
	
	public double getradius(){			//return the radius of center star
		return centerstar.getRadius();
	}
	
	public void setsnowflake(BasicStar center, int depth){	//its allowing user to set snow flake on every place on the board
		centerstar = center;
		this.depth = depth;
	}
	
	public void draw(){					//drawing the snow flake on the user screen
		if (depth == 1){
			centerstar.draw();
		}
		else{
			draw(centerstar, depth);
		}
	}
	
	private void draw(BasicStar base, int depth){	//this is the recursive private call for helping the draw() method
		if (depth>1){
			base.draw();
			Pixel forwardpixels = new Pixel (base.getCenter().getX() + base.getRadius(), base.getCenter().getY());
			BasicStar forwardstar = new BasicStar(forwardpixels, base.getRadius()/3);
			for (int i=0; i<6 ; i = i+1){
				draw(forwardstar, depth-1);
				forwardstar.draw();
				forwardpixels.rotateRelativeToPixel(base.getCenter(), (2*Math.PI)/6);
				forwardstar.setstar(forwardpixels, base.getRadius()/3);
			}
		}
	}
	
}
