
public class KochSnowflake {	//this class sets the koch snow flake
	private Pixel down;			//this field is the base of the triangle
	private Pixel upright;		//the upper and right vertex of the triangle
	private Pixel upleft;		//the upper and left vertex of the triangle
	
	public KochSnowflake(){		//the empty constructor
		Pixel center = new Pixel(Painter.getFrameWidth()/2,Painter.getFrameHeight()/2);
		this.down = new Pixel (Painter.getFrameWidth()/2,40);
		this.upright = new Pixel (Painter.getFrameWidth()/2,40);
		upright.rotateRelativeToPixel(center, 2*Math.PI/3);
		this.upleft = new Pixel (upright.getX(),upright.getY());
		upleft.rotateRelativeToPixel(center, 2*Math.PI/3);
	}
	
	public void draw(){			//this method is drawing the shape of the koch snow flake
		KochCurve upper = new KochCurve (upleft,upright,4);
		KochCurve right = new KochCurve (upright,down,4);
		KochCurve left = new KochCurve (down,upleft,4);
		left.draw();
		upper.draw();
		right.draw();
	}
	

}
