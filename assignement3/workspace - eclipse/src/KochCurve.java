
public class KochCurve {	//this class sets methods and fielsd of koch curve

	private Pixel start;	
	private Pixel end;
	private int depth;
	
	public KochCurve (){	//the empty constructor
		double height = Painter.getFrameHeight();
		double width = Painter.getFrameWidth();
		this.start = new Pixel (width/2, height/2);
		this.end = new Pixel (width/4*3,height/4*3);
		this.depth = 4;
	}
	
	public KochCurve (Pixel start, Pixel end, int depth){	//the non empty constructor, it
		this.start = new Pixel (start.getX(), start.getY());
		this.end = new Pixel (end.getX(),end.getY());
		this.depth = depth;
	}
	
	public void draw(){		//this method is drawing the kochcurve
		draw(start, end, 1, depth);	
		
	}
	
	private void draw(Pixel spixel, Pixel epixel, int i, int depth){	//this is the recursive function which do the "work" for the draw() method
	
			double xstart = spixel.getX();
			double ystart = spixel.getY();
			double xend = epixel.getX();
			double yend = epixel.getY();
			
			Pixel p3 = new Pixel(xstart+(xend-xstart)/3,ystart+(yend-ystart)/3);
			Pixel p4 = new Pixel(xstart+(xend-xstart)/3*2,ystart+(yend-ystart)/3*2);
			Pixel p5 = new Pixel(p4.getX(),p4.getY());
			p5.rotateRelativeToPixel(p3,Math.PI/3);
			Pixel p2 = new Pixel(xstart+(xend-xstart),ystart+(yend-ystart));

			if (i==depth){
				Painter.drawLine(spixel, p3);
				Painter.drawLine(p3, p5);
				Painter.drawLine(p5, p4);
				Painter.drawLine(p4, p2);	
			}
			else{														//the recursive call
				draw(spixel, p3, i+1, depth);
				draw(p3, p5, i+1, depth);
				draw(p5, p4, i+1, depth);
				draw(p4, p2, i+1, depth);
				}
				
	}
}
