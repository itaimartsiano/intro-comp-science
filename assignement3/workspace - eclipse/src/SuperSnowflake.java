
public class SuperSnowflake {		//this class sets the fields and method of super snow flake in order to draw it

	private Snowflake snowflake;	//this field set the central snow flake
	private int depth;				//this field sets the depth of the super snow flake (number of circles)

	public SuperSnowflake(){		//the empty constructor
		snowflake = new Snowflake ();
		depth = 4;
	}
	
	public Snowflake getsnowflake(){ //this method return the snow flake values of the super snow flake
		return snowflake;
	}
	
	public int getsupsnowflakedepth(){	//its return the depth of the super snow flake
		return depth;
	}
	
	public void draw(){					//drawing the super snow flake
		if (depth == 1)	snowflake.draw();
		else	draw(snowflake, depth, snowflake.getcenterpixel());
	}
	
	private void draw(Snowflake base, int depth, Pixel basecenter){	//a recursive call that helps the draw() method
		if (depth>1){
			base.draw();
			Pixel forwardpixels = new Pixel (base.getcenterpixel().getX() + base.getradius()*3, base.getcenterpixel().getY());	
			BasicStar forwardstar = new BasicStar(forwardpixels, base.getradius()/2); 
			Snowflake forwardsnowflake = new Snowflake(forwardstar, base.getdepth());
			for (int i=0; i<6 ; i = i+1){	// this is drawing all the 6 snow flakes around it
				forwardsnowflake.draw();
				forwardpixels.rotateRelativeToPixel(basecenter, (2*Math.PI)/6);
				forwardstar.setstar(forwardpixels, base.getradius()/2);
				forwardsnowflake.setsnowflake(forwardstar, base.getdepth());
			}
			draw(forwardsnowflake, depth-1, basecenter);		//the recursive call
		}
	}
}
