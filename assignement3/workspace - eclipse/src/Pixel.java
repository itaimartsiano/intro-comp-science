
public class Pixel {	//this class sets 2 dimension point on the screen.
	
	private double x;	//describes�the x�coordinate�of the pixel
	private double y;	//describes�the y�coordinate�of the pixel
	
	public Pixel() {	//the empty constructor
		x=0;
		y=0;
	}
	
	public Pixel(double x, double y){	//constructor that allow you insert value manually
		this();
		if (x<=Painter.getFrameWidth() && x>=0 && y<=Painter.getFrameHeight() && y>=0) {
			this.x = x;
			this.y = y;
		}
	}
	
	public double getX(){	//return the x value
		 return x;
	}
	
	public double getY(){	//return the y value
		return y;
	}
	
	public void translate(Pixel p){		//this function move your pixel to another pixel
		this.x = p.x;
		this.y = p.y;
	}
	
	public void rotateRelativeToAxesOrigin(double theta){		//this function rotate (counter clockwise) the pixel relative axes origin (0,0)
		double tempx = this.x;	//use to save the x to y calculate;
		x = (x * Math.cos(theta) - y * Math.sin(theta));
		y = (tempx * Math.sin(theta) + y * Math.cos(theta));
	}
	
	public void rotateRelativeToPixel(Pixel p1,double theta){	//this fuction rotate (counter clockwise) the pixel relative to another pixel
		double tempx = x;	//use to save the x to y calculate;
		x = ((x-p1.x) * Math.cos(theta) - (y-p1.y) * Math.sin(theta) + p1.x);
		y = ((tempx-p1.x) * Math.sin(theta) + (y-p1.y) * Math.cos(theta)+ p1.y);
	}
	

}
