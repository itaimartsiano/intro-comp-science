
public class BasicStar {	//this class sets a basic star
	
	private Pixel center;	//this field save the center pixel's
	private double radius;	//this field save the radius of the basic star
	
	public BasicStar(){		//the empty constructor
		double height = Painter.getFrameHeight()/2;
		double width = Painter.getFrameWidth()/2;
		this.center = new Pixel (width, height);
		double maxRadius = Math.min(width, height)/2;
		this.radius = maxRadius/4;
	}
	
	public BasicStar(Pixel center, double radius){		//the non empty constructor - manually inserting
		this();
		if (radius>=0 && center != null){
			this.center = center;
			this.radius = radius;	
		}	
	}
	
	public void setstar(Pixel center, double radius){	//this setter allow you to insert basic star manually
		this.center = center;
		this.radius = radius;	
	}
	
	public Pixel getCenter(){		//this method return the basic star center
		return center;
	}
	
	public double getRadius(){	//this return the basic star radius
		return radius;
	}
	
	public void draw(){		//this method draw the basic star (using the painter)
		Pixel pradius = new Pixel (center.getX() +radius, center.getY());
		for (int i=0; i<6 ; i= i+1){
			Painter.drawLine(center, pradius);
			pradius.rotateRelativeToPixel(center, (2*Math.PI)/6);
		}	
	}
	
}
