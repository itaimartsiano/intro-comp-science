
public class ReversiPlay {

		public static void main(String[] args) {
		
		}

		public static void printMatrix(int[][] matrix) {            //the function will print matrix
		  	
			if (matrix != null){
		  		for (int i=0; i<matrix.length ;i=i+1){
		  			for (int j=0; j<matrix[i].length; j=j+1){
		  				System.out.print(matrix[i][j]+" ");
		  			}
				System.out.println();
		  		}
		  	}
		}

		public static boolean isEqual(int[][] matrix1, int[][] matrix2) {    /*the function will get 2 matrix of numbers and return if they
		 are equals in thier values and the size of them*/
			
			boolean ans = true;
		  	
			if (matrix1 != null && matrix2 !=null){		// check if the matrix point to null
		  		if (matrix1.length==matrix2.length){
		  			for (int i=0; i<matrix1.length && ans!=false; i=i+1){
		  				if (matrix1[i].length==matrix2[i].length){
		  					for (int j=0; j<matrix1.length && ans!=false; j=j+1){
		  						if (matrix1[i][j]!=matrix2[i][j])
		  							ans=false;
		  					}
		  				}else ans=false;
		  			}	
		  		}
			else ans=false;
			
			return ans;
		  	}
		  	else return false;
		}

		
		  	public static int[][] copyMatrix(int[][] matrix) {   		//the function get matrix and return a copy of it in new place
		  		
				if (matrix != null){			// check if the matrix point to null
					int [][] matrixc = new int [matrix.length][];
					for (int i=0; i<matrix.length; i=i+1){
						int[] arrt = new int[matrix[i].length];
						for (int j=0; j<matrix[i].length; j=j+1){
							arrt[j] = matrix[i][j];
						}
						matrixc[i] = arrt;
					}
					return matrixc;
				}
				else return matrix;	
			}

		  	
		public static int[][] createBoard(int size) {		//the function will create board in size of (size*size) (size must be between 4-40 & only even numbers)
			
			int [][] matrix = null;
			
			if (4<=size && size<=40 && size%2 == 0 ){
				matrix = new int [size][size];
				for (int i=0; i<size ; i++){
					for (int j=0; j<size ; j++){
						matrix [i][j] = 0;
					}
				}
			
				matrix[(size/2)-1][(size/2)-1] = 2;		//this set the start numbers in the center of game board
				matrix[(size/2)-1][(size/2)] = 1;
				matrix[(size/2)][(size/2)-1] = 1;
				matrix[(size/2)][(size/2)] = 2;
			}	
			return matrix;
		}
		
		

		public static boolean isLegal(int[][] board, int player, int row, int column) { 	//this function will check if the move is legal (row&column between 0 - (board.length-1))
			
			boolean ans = false;
			int smax = board.length;	//the max size of the game board
			
			if (inboard(smax,row,column) && board[row][column]==0 ){
				if (lineisLegal(board,player,row,column,row-1,column) || lineisLegal(board,player,row,column,row,column+1) || lineisLegal(board,player,row,column,row,column-1) ||
					lineisLegal(board,player,row,column,row-1,column+1) || lineisLegal(board,player,row,column,row-1,column-1) || lineisLegal(board,player,row,column,row+1,column-1)||
					lineisLegal(board,player,row,column,row+1,column+1) || lineisLegal(board,player,row,column,row+1,column))
					ans = true;
			}
			return ans;
		}

		
		
		
		public static int[][] play(int[][] board, int player, int row, int column) {		//this function will move the washers if the move is legal (play the game)
			
			if (isLegal(board,player,row,column)){
				lineChange(board,player,row,column,row+1,column);		//check down
				lineChange(board,player,row,column,row+1,column+1);		
				lineChange(board,player,row,column,row,column+1);		//check right
				lineChange(board,player,row,column,row-1,column+1);
				lineChange(board,player,row,column,row-1,column);		//check up
				lineChange(board,player,row,column,row-1,column-1);
				lineChange(board,player,row,column,row,column-1);		//check left
				lineChange(board,player,row,column,row+1,column-1);
			}
			return board;
		}
		
		
		public static int benefit(int[][] board, int player, int row, int column) {		// this function will find how much washers are going to change to whasher's player
			
			int count = 0;
			int countc = 0;
			int [][] boardc = copyMatrix(board);		// make a copy of the board - to check on it
			
			if (isLegal(board,player,row,column)){
				play(boardc,player,row,column);
				for (int i=0; i<boardc.length ;i=i+1){		//count amount of washer in the new board
					for (int j=0; j<boardc[i].length; j=j+1){
						if (board[i][j] == player)
							countc = countc+1;
					}
				}
				for (int i=0; i<board.length ;i=i+1){		//count amount of washer in the old board
					for (int j=0; j<board[i].length; j=j+1){
						if (board[i][j] == player)
							count = count+1;
					}
				}
				count = countc - count;
			}
			return count;
		}

		
		
		public static int[][] possibleMoves(int[][] board, int player) {	//this function will find all the possible moves
			
			int count = movescounter(board,player);		//use to count how many possible moves are legal, we need it to the length of matrix
			int [][] matrix = new int [count][2];
			
			if (count>0){
			count=0;
			for (int i=0; i<board.length ;i=i+1){	
				for (int j=0; j<board[i].length; j=j+1){
					if (isLegal(board,player,i,j)){
						matrix[count][0] = i;
						matrix[count][1] = j;	
						count = count + 1;
						
					}
				}
			}
			}
			return matrix;
		}

		
		
		public static boolean hasMoves(int[][] board, int player) {		// this function will find if there is possible moves to player
			
			boolean ans = false;
			
			for (int i=0; i<board.length && ans!=true;i=i+1){	
				for (int j=0; j<board[i].length && ans!=true; j=j+1){
					if (isLegal(board,player,i,j))
						ans=true;	
				}
			}
			return ans;
		}

		
		
		public static int findTheWinner(int[][] board) {	//this function will find the winner in the game board
			
			int playera = 0;	//use to count how many washers of player 1 on the board
			int playerb = 0;	//use to count how many washers of player 2 on the board
			int winner = 0;		//if it equal it will return zero
			
			for (int i=0; i<board.length ;i=i+1){		
				for (int j=0; j<board[i].length; j=j+1){
					if (board[i][j] == 1)
						playera = playera + 1;
					else if (board[i][j] == 2)
							playerb = playerb + 1;
				}
			}
			if (playera > playerb)
				winner = 1;
			else if (playerb > playera)
					winner = 2;
				else if (playera==playerb)
						winner = 0;
			
			return winner;
		}

		
		
		public static boolean gameOver(int[][] board) {	//this function will check if the game is over
			
			boolean ans = false;
			int playera = 0;	//use to count how many washers of player 1 on the board
			int playerb = 0;	//use to count how many washers of player 2 on the board
			int boardsize = board.length;
			
			if (!hasMoves(board,1) && !hasMoves(board,2))	ans = true;
			else {
				for (int i=0; i<boardsize ;i=i+1){		
					for (int j=0; j<boardsize; j=j+1){
						if (board[i][j] == 1)
							playera = playera + 1;
						else if (board[i][j] == 2)
								playerb = playerb + 1;		
					}
				}
				if (playera+playerb == boardsize*boardsize || playera == 0 || playerb == 0)	ans = true;
			}
			return ans;
		}

		
		
		public static int[] randomPlayer(int[][] board, int player) {	// choose a random move for player
				
			if (hasMoves(board,player)){
				int [] ans =  new int [2];
				int [][] matrix = possibleMoves(board,player);
				int num = (int) (Math.random() * matrix.length);
				ans [0] = matrix [num][0];
				ans [1] = matrix [num][1];
			
			return ans;
			}
			
			else return null;

		}


		public static int[] greedyPlayer(int[][] board, int player) {		//this function will choose the most beneficial move in the board (get maximum washers);
			
			int[][] posmo = null;							//save all the possible moves
			int[] ans = null;
			
			if (hasMoves(board,player)){
				ans = new int [2];
				posmo = possibleMoves(board,player);
				if (posmo.length == 1){						//if there is only one option to play
					ans [0] = posmo[0][0];
					ans [1] = posmo[0][1];
				}
				else{
					int count = 1;													// use to count the amount of maximum numbers (if there is equals maximum numbers)
					int benfmax = benefit(board,player,posmo[0][0],posmo[0][1]);	//save the max move benefit
					int benfsearch = 0;												//benefit of move (variable)
					for (int i = 1; i<posmo.length ; i = i+1){						//counting how many max numbers and save the value of them
						benfsearch = benefit(board,player,posmo[i][0],posmo[i][1]);
						if (benfsearch > benfmax){
							count = 1;
							benfmax = benfsearch;
						}
						else
							if (benfmax == benfsearch)
								count = count+1;
					}
					if (count == 1){												//if there is only one max number
						int i = 0;
						while (benfsearch!= (-1)){
							benfsearch = benefit(board,player,posmo[i][0],posmo[i][1]);
							if (benfsearch == benfmax){
								ans [0] = posmo[i][0];
								ans [1] = posmo[i][1];
								benfsearch = (-1);
							}
						}
					}
					else{															// if there is more then 1 max numbers
						int[][] multianswer = new int [count][2];
						int i = 0;
						int row = 0;
						while (count!=0){
							benfsearch = benefit(board,player,posmo[i][0],posmo[i][1]);
							if (benfsearch == benfmax){
								multianswer [row][0] = posmo[i][0];
								multianswer [row][1] = posmo[i][1];
								count = count - 1;
								row = row + 1;
							}
						}
						
						row = (int) (Math.random() * multianswer.length);		// choose one of the option randomalic
						ans [0] = posmo[row][0];
						ans [1] = posmo[row][1];
					}
				}
				}
			return ans;
		}
			
		
		
		public static int[] defensivePlayer(int[][] board, int player) {	/*this function will calculate what is the beneficial move if the other player will act in a greedy move 
			after the first player will do move. */
			
			int [] ans = new int [2];
			
			if (hasMoves(board,player)){					// check if there is a possible moves
			
				int [][] posmove = possibleMoves(board, player);
				int winplay = -1600;						// save the value of the highest benefit move.
				int [][] multianswer = new int [160][2];
				int playerb;								//save the number of second player washer.
				int benefitplayera;
				int benefitplayerb;
				int count = 0;								//counting the number of maximum benefit moves
				int [][] newboard;							//copy of board (use to make the calculate
			
				if (player == 1)							//make player b washer
					playerb = 2;
				else	playerb = 1;
				
				for (int i=0; i<posmove.length; i = i+1){		// calculate what is the beneficial move
					newboard = copyMatrix(board);
					benefitplayera = benefit(board,player,posmove[i][0],posmove[i][1]);	//count benefit of first play
					play(newboard,player,posmove[i][0],posmove[i][1]);
					int [] playerbplay= greedyPlayer(newboard,playerb);
					benefitplayerb = benefit(newboard,playerb,playerbplay[0],playerbplay[1]);
					if (benefitplayera-benefitplayerb > winplay || i==0){
						count = 0;
						winplay = benefitplayera-benefitplayerb;
						multianswer[count][0] = posmove[i][0];
						multianswer[count][1] = posmove[i][1];
					}
					else
						if (benefitplayera-benefitplayerb == winplay){
							count = count + 1;
							multianswer[count][0] = posmove[i][0];
							multianswer[count][1] = posmove[i][1];
						}
				}
				if (count == 0){	
					ans [0] = multianswer[0][0];
					ans [1] = multianswer[0][1];
				}
				else{
					int row = (int) (Math.random() * (count+1));		// choose one of the option randomalic if there is equal answers 
					ans [0] = multianswer[row][0];
					ans [1] = multianswer[row][1];
				}
				return ans;
			}
			else return null;
			
		}

		public static int[] byLocationPlayer(int[][] board, int player) {		//this function will play in moves by location, allways prefer the farrest possible move from the center (not diagonal way)
			
			int [][] posmove = possibleMoves(board, player);
			int winplay = 0;					
			int [][] multianswer = new int [160][2];
			int distance = 0;
			int count = 0;
			int [] ans = new int [2];
			
			if (hasMoves(board,player)){	
				for (int i=0; i<posmove.length; i = i+1){		
					distance = distfromcenter (board, posmove[i][0], posmove[i][1]);
					if (distance > winplay){
						count = 0;
						winplay = distance;
						multianswer[count][0] = posmove[i][0];
						multianswer[count][1] = posmove[i][1];
						
					}
					else
						if (distance == winplay){
							count = count + 1;
							multianswer[count][0] = posmove[i][0];
							multianswer[count][1] = posmove[i][1];
						}
				}
				if (count == 0){	
					ans [0] = multianswer[0][0];
					ans [1] = multianswer[0][1];
				}
				else{
					int row = (int) (Math.random() * (count+1));		// choose one of the option randomalic
					ans [0] = multianswer[row][0];
					ans [1] = multianswer[row][1];
				}
				return ans;
			}else return null;
			
		}

		public static int[] myPlayer(int[][] board, int player) {
				
			return null;
		}
	
		
		public static boolean inboard(int smax, int row, int column) {	//checking if the move is on the board
			
			boolean ans = false;
			int smin = -1;
			
			if (row<smax && row>smin && column<smax && column>smin)
				ans = true;
					
			return ans;
		}
		
		
		
		public static boolean lineisLegal(int[][] board, int player, int row, int column, int nextr, int nextc) {	//function will check if the move is legal in specific line
				
				int smax = board.length;
				boolean ans = false;
				while (inboard(smax,row,column) && inboard(smax,nextr,nextc) && ans!=true && board[nextr][nextc]!=0 && board[nextr][nextc]!=player){
					int tmpr = row;
					int tmpc = column;
					row=nextr;
					column=nextc;
					nextc=nextc + (nextc-tmpc);
					nextr = nextr + (nextr-tmpr);
					if (inboard(smax,nextr,nextc) && board[nextr][nextc]==player)
						ans = true;
				}
				
				return ans;
		}

		
		
		public static int[][] lineChange(int[][] board, int player, int row, int column, int rowt, int columnt) {	//the function will change the washers in specific line if it was legal
			
			int smax = board.length;
			boolean ans = lineisLegal(board,player,row,column,rowt,columnt);
			
			while (ans && inboard(smax,rowt,columnt) && inboard(smax,row,column) && board[rowt][columnt]!=0 && board[rowt][columnt]!=player){
				board[row][column] = player;
				board[rowt][columnt] = player;
				int tmpr = row;
				int tmpc = column;
				row=rowt;
				column=columnt;
				columnt=columnt + (columnt-tmpc);
				rowt = rowt + (rowt-tmpr);
				
			}
			return board;
		}
		
		

		public static int distfromcenter (int [][] board,int row,int column) {		//the function will return the distance from center (from the 4 square in the center to the side - without diagonals)
			
			int ans = 0;
			int countrow = 0;
			int countcolumn = 0;
			
			if (row < (board.length/2))
				countrow = (board.length/2)-(row) ;
			else
				if (row >= (board.length/2))
					countrow =	((row+1)-(board.length/2));
		
					
			if (column < (board[row].length/2))
				countcolumn = ((board[row].length/2)-(column+1));
			else
				if (column >= (board[row].length/2))
					countcolumn = ((column)-(board[row].length/2));
				
			ans = countcolumn + countrow;		
			
			return ans;
		
		}

		
		public static int movescounter(int[][] board, int player) {		//function will find how many moves in the board for player
			
			int count = 0;
			for (int i=0; i<board.length ;i=i+1){		//count the numbers of possible moves
				for (int j=0; j<board[i].length; j=j+1){
					if (isLegal(board,player,i,j))
						count = count + 1;
				}
			}
			return count;
		}
		
	}